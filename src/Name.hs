
module Name where

import Data.Data
import Data.String
import Data.Text (Text)

import Refresh

-- | Name for value or type (kinds can't be abstracted over).
--
data Name i = Name
  { nameRaw  :: Text  -- ^ the name itself
  , nameUid  :: Int   -- ^ uuid
  , nameInfo :: i     -- ^ location in file, unless generated
  }
  deriving stock (Eq, Ord, Data)

refresh :: MonadRefresh m => Name i -> m (Name i)
refresh (Name n _ i) = do
  uid <- uuid
  return $ Name n uid i

instance IsString (Name ()) where
  fromString n = Name (fromString n) 0 ()

isBound :: Name i -> Bool
isBound n = nameUid n == 0
