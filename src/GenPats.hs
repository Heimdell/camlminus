{- | Generator for pattern synonyms for automatic hiding of ``Control.Monad.Free`
     injection.
-}
module GenPats where

import Control.Monad

import Data.Traversable

import Language.Haskell.TH

-- | For each plain constructor, generates a pattern synonym (w/o type sig).
--
genPats :: Name -> Q [Dec]
genPats name = do
  info <- reify name
  Just free  <- lookupValueName "Free"
  case info of
    TyConI (DataD _ _tname _args _kind cons _) -> do
      join <$> for cons \case
        NormalC n args -> do
          args' <- for args \_ -> newName "a"
          let patName = mkName $ init $ nameBase n
          return
            [ PatSynD patName (PrefixPatSyn args') ImplBidir
            $ ConP free [ConP n $ map VarP args']
            ]

        -- InfixC _ n _ -> do
        --   l' <- newName "l"
        --   r' <- newName "r"
        --   let patName = mkName $ init $ nameBase n
        --   return
        --     [ PatSynD patName (InfixPatSyn l' r') ImplBidir
        --     $ ConP free [InfixP (VarP l') n (VarP r')]
        --     ]

        c -> error $ "Constructor shape <" ++ show c ++ "> is not supported"

    c -> error $ "Is not a data declaration - <" ++ show c ++ ">"
