{-# OPTIONS_GHC -Wno-missing-signatures -Wno-missing-pattern-synonym-signatures #-}

{- | Abstract syntax tree.

     The parameter `i`  is used for information (position in file, etc).

     All `Free`-structures are made this way so we get automatic substitution.
-}
module AST where

import Control.Monad.Free

import Data.Data
import Data.Generics.Uniplate.Data
import Data.Text (Text)

import GHC.Generics

import Name
import GenPats

-- | A program.
--
type AST i = Free (AST_ i) (Name i)

-- | One layer of program.
--
data AST_ i self
  = Lam_    i (Name i) (Type i) self     -- ^ \(a : A) -> a
  | LAM_    i (Name i) (Kind i) self     -- ^ /\A -> \(a : A) -> a
  | App_    i self self                  -- ^ f x
  | APP_    i self (Type i)              -- ^ f @T

  -- | Symbol_ Name Type                 --   (Cons : a -> List a -> List a)
  | Make_   i (Name i) [(Name i, Maybe self)]  -- ^ Cons { head = x, tail = map f xs },
                                         --   always saturated
  | Upd_    i  self    [(Name i, Maybe self)]  -- ^ Cons { head = x, tail = map f xs },
  | Match_  i self [Alt i self]          -- ^ case x of { 1 -> ...; 2 -> ... }
  | Get_    i self (Name i)              -- ^ foo.bar

  | Lit_    i Lit                        -- ^ 1, 2.0, "3"

  | Letrec_ i [Decl i self] self         -- ^ let x = 1; data Id a = Id { getId :: a } in Id 1
                                         --   that example should not typecheck somehow
  deriving stock (Functor, Foldable, Traversable, Generic, Data)

-- | A type or value declaration.
--
data Decl i ast
  = Value i (Name i) (Type i) ast       -- ^ x : Int = 1
  | Type  i (Name i) (Kind i) [Ctor i]  -- ^ data Maybe a = Just { fromJust :: a } | Nothing
  deriving stock (Functor, Foldable, Traversable, Data)

-- | A constructor of userspace type.
--
data Ctor i = Ctor
  { ctorInfo   :: i                   -- ^ location
  , ctorName   :: Name i              -- ^ Cons :
  , ctorCtx    :: Maybe (Type i)      -- ^ (Eq a) =>
  , ctorFields :: [(Name i, Type i)]  -- ^ { x : a, xs : List a }
  , ctorType   :: Type i              -- ^ -> List Int a
  }
  deriving stock (Data)

-- | A branch of case-expression.
--
data Alt i self = Alt
  { altInfo    :: i           -- ^ location
  , altPattern :: Pattern i   -- ^ Cons { x = x, xs = xs }
  , altGuard   :: Maybe self  -- ^ when elem x ys
  , altBody    :: self        -- ^ -> + x 1
  }
  deriving stock (Functor, Foldable, Traversable, Data)

type Pattern i = Free (Pattern_ i) (Name i)

data Pattern_ i self
  = IsCtor_     i (Name i) [(Name i, Maybe self)]  -- ^ Ctor { x = x, xs = xs } ->
  | IsLit_      i  Lit                       -- ^ 1 ->
  | IsAnything_ i                            -- ^ _ ->
  deriving stock (Functor, Foldable, Traversable, Generic, Data)

type Type i = Free (Type_ i) (Name i)

data Type_ i self
  = TArrow  i self self                     -- ^ a -> b
  | TApp    i self self                     -- ^ f x
  | Sym_    i (Name i)                      -- ^ #name, Get emits `HasName #name ty`
  | Forall_ i [(Name i, Kind i)] self self  -- ^ forall (n :: k). ctx => body
  deriving stock (Functor, Foldable, Traversable, Generic, Data)

data Kind i
  = Star   i                   -- ^ *
  | KArrow i (Kind i) (Kind i) -- ^ (* -> *) -> *
  | Constraint i               -- ^ ?, for `Eq Int`
  deriving stock (Data)

data Lit
  = S Text
  | F Double
  | I Integer
  deriving stock (Data)

-- | This pattern will align other `AST` patterns below to use correct vars.
--
pattern Var :: Name i -> Free (AST_ i) (Name i)
pattern Var a = Pure a

genPats ''AST_

-- | This pattern will align other `Pattern` patterns below to use correct vars.
--
pattern IsVar :: Name i -> Free (Pattern_ i) (Name i)
pattern IsVar a = Pure a

genPats ''Pattern_

-- | This pattern will align other `Type` patterns below to use correct vars.
--
pattern TVar :: Name i -> Free (Type_ i) (Name i)
pattern TVar a = Pure a

genPats ''Type_

mapTypes :: (Typeable i, Data i) => (Type i -> Type i) -> AST i -> AST i
mapTypes = transformBi

mapPatterns :: (Typeable i, Data i) => (Pattern i -> Pattern i) -> AST i -> AST i
mapPatterns = transformBi

mapPatternNames :: (Typeable i, Data i, Data v) => (Name i -> Name i) -> Free (Pattern_ i) v -> Free (Pattern_ i) v
mapPatternNames = transformBi

traverseTypes :: (Monad m, Typeable i, Data i) => (Type i -> m (Type i)) -> AST i -> m (AST i)
traverseTypes = transformBiM
