
module Subst where

import Control.Monad.Free

import Data.Map qualified as Map
import Data.Map (Map)

import Refresh
import Name

-- | A substitution for variables (will be used in typecheck).
--
newtype Subst f v = Subst { getSubst :: Map v (Free f v) }

-- | Apply a substitution.
--
subst :: (Functor f, Ord v) => Subst f v -> Free f v -> Free f v
subst (Subst s) prog = do
  var <- prog
  case Map.lookup var s of
    Just res -> res
    Nothing  -> return var

-- | Apply a substitution to one variable.
--
substVar :: (Functor f, Ord v) => Subst f v -> v -> v
substVar (Subst s) var = do
  case Map.lookup var s of
    Just (Pure v) -> v
    _             -> var

-- | When composing substitutions, the left one applies to all types from right one.
--
instance (Functor f, Ord v) => Semigroup (Subst f v) where
  Subst s <> Subst t = Subst $ s <> Map.map (subst (Subst s)) t

instance (Functor f, Ord v) => Monoid (Subst f v) where
  mempty = Subst mempty

capture :: (Functor f, MonadRefresh m, Ord i) => Name i -> m (Subst f (Name i))
capture n = do
  n' <- refresh n
  return $ Subst $ Map.singleton n (Pure n')
