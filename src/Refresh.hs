
module Refresh where

import Control.Monad
import Control.Monad.State

class Monad m => MonadRefresh m where
  uuid :: m Int

newtype Fresh = Fresh Int

instance Monad m => MonadRefresh (StateT Fresh m) where
  uuid = do
    Fresh uid <- get
    put (Fresh (uid + 1))
    return (uid + 1)

instance {-# overlappable #-} (MonadTrans t, Monad m, Monad (t m), MonadRefresh m) => MonadRefresh (t m) where
  uuid = lift uuid
