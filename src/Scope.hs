
module Scope where

import Control.Arrow (second)

import Data.Data
import Data.Traversable
import Data.Foldable

import Name
import AST
import Subst
import Refresh

lam :: (MonadRefresh m, Ord i) => i -> Name i -> Type i -> AST i -> m (AST i)
lam i n ty body = do
  s <- capture n
  return $ Lam i (substVar s n) ty (subst s body)

lAM :: (MonadRefresh m, Ord i, Data i) => i -> Name i -> Kind i -> AST i -> m (AST i)
lAM i n k body = do
  s <- capture n
  return $ LAM i (substVar s n) k (mapTypes (subst s) body)

type ADecl i =
  ( Decl i (AST i)
  , Subst (AST_     i) (Name i)
  , Subst (Type_    i) (Name i)
  , Subst (Pattern_ i) (Name i)
  )

letrec
  :: forall i m
  .  ( MonadRefresh m
     , Ord i
     , Data i
     )
  => i
  -> [ADecl i]
  -> AST i
  -> m (AST i)
letrec i ads b = do
  let ds =         [d | (d, _, _, _) <- ads]
  let ss = mconcat [s | (_, s, _, _) <- ads]
  let ts = mconcat [t | (_, _, t, _) <- ads]
  let ps = mconcat [p | (_, _, _, p) <- ads]

  let full = mapTypes (subst ts) . (mapPatterns . mapPatternNames) (substVar ps) . subst ss

  let
    ds' = flip map ds \case
      Value vi n t bv ->
        --                       +-- is not substituted!
        --                       v
        Value vi (substVar ss n) t (full bv)

      Type ti n k cs ->
        Type ti (substVar ts n) k
          $ flip map cs \case
            Ctor ci cn ctx fs t ->
              --                       +-- is not substituted!
              --                       v
              Ctor ci (substVar ss cn) ctx (map (second (subst ts)) fs) (subst ts t)

  return $ Letrec i ds' (full b)

value :: (MonadRefresh m, Ord i, Data i) => i -> Name i -> Type i -> AST i -> m (ADecl i)
value i n t b = do
  s <- capture n
  return (Value i n t b, s, mempty, mempty)

type_ :: (MonadRefresh m, Ord i, Data i) => i -> Name i -> Kind i -> [Ctor i] -> m (ADecl i)
type_ i n k cs = do
  t <- capture n
  s <- mconcat <$> for cs \case
    Ctor _ cn _ _ _ -> capture cn
  p <- mconcat <$> for cs \case
    Ctor _ cn _ _ _ -> capture cn
  return ( Type i n k cs, s, t, p)

alt :: forall i m. (MonadRefresh m, Ord i, Data i) => i -> Pattern i -> Maybe (AST i) -> AST i -> m (Alt i (AST i))
alt i pat grd b = do
  ss <- capturePat pat
  ps <- capturePat pat
  return $ Alt i (subst ps pat) (subst ss <$> grd) (subst ss b)

capturePat :: (MonadRefresh m, Ord i, Data i, Functor f) => Pattern i -> m (Subst f (Name i))
capturePat pat = mconcat <$> for (toList pat) capture

forall_ :: (MonadRefresh m, Ord i, Data i) => i -> [(Name i, Kind i)] -> Type i -> Type i -> m (Type i)
forall_ i args ctx t = do
  ts <- mconcat <$> for [n | (n, _) <- args] capture
  return $ Forall i [(substVar ts n, k) | (n, k) <- args] (subst ts ctx) (subst ts t)
